package com.zenika.academy;
import com.zenika.academy.quiz.questions.*;
import com.zenika.academy.quiz.Player;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class QuizManager {
    public static void main(String[] args)  {
        File fileApiKey = new File("src/main/resources/tmdb-api-key.txt");
        Random r = new Random();

       /* List<Question> questions = List.of(
                new OpenQuestion("Comment s'appelle le chien d'Obélix ?", "Idéfix"),
                new MultipleChoiceQuestion("De quelle couleur est le cheval blanc d'Henri IV ?", List.of("Bleu", "Rouge"), "Blanc", r),
                new TrueFalseQuestion("Milou est en fait un chat", false)
        );*/

        QuestionFactory questionFactory = QuestionFactory.getInstance(fileApiKey);


      List<Question> questions  = new ArrayList<Question>();
      //mettre  l'instance + la methode  et voir avec ifPresent si question qui est égal à instance+methode , existe .
        // Si ça existe tu me fait liste de question add la question
        questionFactory.generateQuestionFromTitle("Titanic", QuestionLevel.HARD ).ifPresent(question -> questions.add(question));
        questionFactory.generateQuestionFromTitle("Titanic", QuestionLevel.MEDIUM ).ifPresent(question -> questions.add(question));
        questionFactory.generateQuestionFromTitle("Titanic", QuestionLevel.EASY ).ifPresent(question -> questions.add(question));

      Scanner sc = new Scanner(System.in);

        Player p = createPlayer(sc);

        for (Question q : questions) {
            p.scorePoints(askQuestion(sc, q));
        }

        System.out.println(p.congratulations());
    }

    private static int askQuestion(Scanner sc, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = sc.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return 2;
            case ALMOST_CORRECT:
                return 1;
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return new Player(userName);
    }
}
