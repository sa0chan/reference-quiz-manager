package com.zenika.academy.quiz.questions;
import com.zenika.academy.tmdb.MovieInfo;
import java.util.Random;
import java.util.*;

public final class RandomListFactory {

    private static volatile RandomListFactory instance = null ;
    protected Random random;
    protected MovieInfo movie;

    private RandomListFactory(MovieInfo movieInfo){

        this.movie = movieInfo ;

    }


    //methode d'accès au singleton
    public final static RandomListFactory getInstance(MovieInfo movieInfo){

        //Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
        //d'éviter un appel coûteux à synchronized,une fois que l'instanciation est faite.

        if (RandomListFactory.instance == null) {
            // Le mot-clé synchronized sur ce bloc empêche toute instanciation
            // multiple même par différents "threads".Il est TRES important.
            synchronized(RandomListFactory.class) {
                if (RandomListFactory.instance == null) {
                    RandomListFactory.instance = new RandomListFactory(movieInfo);
                }
            }
        }

        RandomListFactory randomListFactory = RandomListFactory.instance;
        return randomListFactory;

    }



    public List<String> getRandomListFactory(MovieInfo movie) {
        random =  new Random();
        Set<String> setIncorrectAnswer = new HashSet<>();
        while (setIncorrectAnswer.size() < 3){
            int randomIndex = random.nextInt(movie.cast.size());
            setIncorrectAnswer.add(movie.cast.get(randomIndex).actorName);
        }

        Iterator<String> i = setIncorrectAnswer.iterator();
        //ou faire un for each  ou encore creer une liste à partir du set new ArrayList<>(setIncorrectAnswer)

        List<String> incorrectAnswer = List.of(i.next(),i.next(),i.next());

        return incorrectAnswer;
    }
}
