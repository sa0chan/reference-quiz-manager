package com.zenika.academy.quiz.questions;

public enum QuestionLevel {

    HARD,
    MEDIUM,
    EASY;

}


