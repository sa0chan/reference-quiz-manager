package com.zenika.academy.quiz.questions;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Random;


// La classe est finale, car un singleton n'est pas censé avoir d'héritier.
public final class QuestionFactory {

    // L'utilisation du mot clé volatile, en Java version 5 et supérieure,
    // permet d'éviter le cas où "Singleton.instance" est non nul,
    // mais pas encore "réellement" instancié.
    private static volatile QuestionFactory instance = null;
    protected TmdbClient tmdbClient ;
    protected  Random random;
    protected int randomIndex;


//constructeur
       private QuestionFactory(File fileApiKey){

               try {
                   tmdbClient =  new TmdbClient(fileApiKey);
               } catch (IOException e) {
                   e.printStackTrace();
               }

            }

    //constructeur mock on peut y acceder sur le même package
     QuestionFactory(int seed, TmdbClient tmdbClient){

        this.tmdbClient = tmdbClient;
        random = new Random(seed);


    }

//methode d'accès au singleton
        public final static QuestionFactory getInstance(File fileApiKey){

            //Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
            //d'éviter un appel coûteux à synchronized,une fois que l'instanciation est faite.

            if (QuestionFactory.instance == null) {
                // Le mot-clé synchronized sur ce bloc empêche toute instanciation
                // multiple même par différents "threads".Il est TRES important.
                synchronized(QuestionFactory.class) {
                    if (QuestionFactory.instance == null) {
                        QuestionFactory.instance = new QuestionFactory(fileApiKey);
                    }
                }
            }

            QuestionFactory questionFactory = QuestionFactory.instance;
            return questionFactory;

            }


    // optional question existe ou pas en fonction du titre
    public Optional<Question> generateQuestionFromTitle(String title, QuestionLevel niveau){

        Optional<MovieInfo> optionalMovieInfo = tmdbClient.getRandomPopularMovie();
        //on appelle la methode getmovie et on stock le résultat dans une var optionnel movieInfo.
        // On recupere les info d'un film à partir de son titre et on a peut être le resultat dans une variable

        /* return optionalMovieInfo.map((MovieInfo movieInfo) -> getQuestionFromMovie(movieInfo));*/
        switch (niveau){
            case HARD:
                return optionalMovieInfo.map(this::getHardQuestionFromMovie);
            case MEDIUM:
                return optionalMovieInfo.map(this::getMediumQuestionFromMovie);
            case EASY:
                return optionalMovieInfo.map(this::getEasyQuestionFromMovie);
            default:
                return Optional.empty();
        }

    }

    public Question getHardQuestionFromMovie(MovieInfo movie){
        return new OpenQuestion("Quelle est la date de sortie du film " + movie.title + " ? " ,  movie.year.toString());
    }

    public Question getMediumQuestionFromMovie(MovieInfo movie){

       RandomListFactory instance =  RandomListFactory.getInstance(movie);
       Random r = new Random();
        return new MultipleChoiceQuestion("Qui n'a pas joué dans le film " + movie.title + " ? ", instance.getRandomListFactory(movie) ,getShuffledName(), r);
    }

    public Question getEasyQuestionFromMovie(MovieInfo movie){
        random =  new Random() ;
/*
        randomIndex = random.nextInt(movie.cast.size());
*/
        return new TrueFalseQuestion("Est ce que " + movie.cast.get(getRandomIndex(movie)).actorName + " joue le rôle de " + movie.cast.get(getRandomIndex(movie)).characterName + " dans "+ movie.title + "  ?", true);
    }

    public String getShuffledName(){

       Optional<MovieInfo> optionalMovieInfo2 = tmdbClient.getRandomPopularMovie();
       return optionalMovieInfo2.get().cast.get(0).actorName;

    }

    public int getRandomIndex(MovieInfo movie){
        if(movie.cast.size() < 3){
            return randomIndex = random.nextInt(movie.cast.size());

        }else{
            return randomIndex = random.nextInt(6);
        }
    }



}
