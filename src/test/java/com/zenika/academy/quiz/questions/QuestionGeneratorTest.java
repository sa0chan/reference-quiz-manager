package com.zenika.academy.quiz.questions;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

class QuestionGeneratorTest {


    @Test
    void testGenerateHardQuestionFromTitle() {
            //Création du movieInfo
            MovieInfo movieInfo = new MovieInfo("titleTest", Year.of(1997),
                    List.of(new MovieInfo.Character("Caledon Hockley","Billy Zane")));


            TmdbClient mockTmdbClient = mock(TmdbClient.class);
            QuestionFactory questionFactory = new QuestionFactory(2,mockTmdbClient);
            when(mockTmdbClient.getMovie("titleTest")).thenReturn(Optional.of(movieInfo));
            //get() permet de sortir de l'optional  .. pour le test prendre que la question
            assertEquals("Quelle est la date de sortie du film titleTest dans lequel joue Billy Zane" , questionFactory.generateQuestionFromTitle("titleTest",QuestionLevel.HARD).get().getDisplayableText());
            assertEquals("CORRECT" , questionFactory.generateQuestionFromTitle("titleTest",QuestionLevel.HARD).get().tryAnswer("1997").toString());

        }

    @Test

    void testGenerateHardQuestionFromTitleReturnEmpty()  {
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        QuestionFactory questionFactory = new QuestionFactory(2,mockTmdbClient);
        when(mockTmdbClient.getMovie("titleTest")).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), questionFactory.generateQuestionFromTitle("lokjj",QuestionLevel.HARD));

    }

    @Test
    void testGenerateMediumQuestionFromTitle() {
        //Création du movieInfo
        MovieInfo movieInfo = new MovieInfo("titleTest", Year.of(1997),
                List.of(new MovieInfo.Character("A","a"),new MovieInfo.Character("B","b"),new MovieInfo.Character("C","c")));


        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        QuestionFactory questionFactory = new QuestionFactory(2,mockTmdbClient);
        when(mockTmdbClient.getMovie("titleTest")).thenReturn(Optional.of(movieInfo));
        //get() permet de sortir de l'optional  .. pour le test prendre que la question
        assertEquals("CORRECT" , questionFactory.generateQuestionFromTitle("titleTest",QuestionLevel.MEDIUM).get().tryAnswer("Brad Pitt").toString());

    }

    @Test

    void testGenerateMediumQuestionFromTitleReturnEmpty()  {
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        QuestionFactory questionFactory = new QuestionFactory(2,mockTmdbClient);
        when(mockTmdbClient.getMovie("titleTest")).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), questionFactory.generateQuestionFromTitle("lo0jj",QuestionLevel.MEDIUM));

    }



}