# Quiz Manager

## Build it

```
mvn clean package
```

## Run it

```
java -jar target/quiz-manager-<version>-jar-with-dependencies.jar
```